# Clase 1 y 2
miVariable = 3
print(miVariable)

miVariable = "todos los dias lo mismo"
print(miVariable)

miVariable = 10.2
print(miVariable)

x = 10
y = 21
z = x + y
print(id(y))

# Clase 3 - Type es para saber que tipo de variable es.
a = False
print(a)
print(type(a))

a = 10.3
print(a)
print(type(a))

a = "es de noche"
print(a)
print(type(a))


a = 10
print(a)
print(type(a))

# manejo de cadenas
miGrupoFavorito = "Guns and Roses"
caracteristicas = "The best "
print("Mi grupo favorito es:", miGrupoFavorito, caracteristicas)

numero1 = "4"
numero2 = "8"
print(int(numero1) + int(numero2))

# Tipos de datos Bools: minuto 27.9